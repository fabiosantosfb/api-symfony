<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;
// use FOS\RestBundle\Controller\Annotations as Route;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use  AppBundle\Entity\usuario;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\GET("/")
     */
    public function indexAction(Request $request)
    {
        $data = ['mensagem' => 'bem vindo'];
        $view = $this->view($data, Response::HTTP_OK);
        return $view;
    }

    /**
    * @Route("/api/v1/usuario/search/{id}")
    * @Method("GET")
    */
    public function idAction($id)
    {
     $singleresult = $this->getDoctrine()->getRepository('AppBundle:usuario')->find($id);
     if ($singleresult === null) {
       return $this->view("Usuario não encontrado", Response::HTTP_NOT_FOUND);
     }
      return $this->view($singleresult, Response::HTTP_OK);
    }

    /**
    * @Route("/api/v1/usuario/add/")
    * @Method("POST")
    */
    public function postAction(Request $request) {
        $user = new usuario();
        $name = $request->get('nome');
        $tipo = $request->get('tipo');
        $email = $request->get('email');
        $senha = $request->get('senha');

        if(empty($name) || empty($tipo) || empty($email) || empty($senha)){
         return $this->view("Preencha os campos corretamente", Response::HTTP_NOT_ACCEPTABLE);
        }
        $user->setNome($name);
        $user->setTipoUser($tipo);
        $user->setEmail($email);
        $user->setSenha($senha);

        $result = $this->getDoctrine()->getManager();
        $result->persist($user);
        $result->flush();

        return $this->view("Usuario Registrado com sucesso", Response::HTTP_OK);
    }

    /**
    * @Route("/api/v1/usuario/update/{id}")
    * @Method("PUT")
    */
    public function updateAction($id, Request $request)
        {
          $result = $this->getDoctrine()->getManager();
          $usuario = $result->getRepository('AppBundle:usuario')->find($id);

            if (!$usuario) {
                return $this->view("Usuario nao encontrado", Response::HTTP_OK);
            }

            $name = $request->get('nome');
            $tipo = $request->get('tipo');
            $email = $request->get('email');
            $senha = $request->get('senha');

            if(!empty($name)){
              $usuario->setNome($name);
            }
            if(!empty($tipo)){
              $usuario->setTipoUser($tipo);
            }
            if(!empty($email)){
              $usuario->setEmail($email);
            }
            if(!empty($senha)){
              $usuario->setSenha($senha);
            }

            $result->flush();

            return $this->view("Usuario Atualizado com sucesso", Response::HTTP_OK);
    }

    /**
    * @Route("/api/v1/usuario/list/")
    * @Method("GET")
    */
    public function listAction()
    {
     $singleresult = $this->getDoctrine()->getRepository('AppBundle:usuario')->findAll();
     if ($singleresult === null) {
       return $this->view("Não há usuarios", Response::HTTP_NOT_FOUND);
     }
      return $this->view($singleresult, Response::HTTP_OK);
    }

    /**
    * @Route("/api/v1/usuario/remover/{id}")
    * @Method("DELETE")
    */
    public function deleteAction($id)
    {
     $result = $this->getDoctrine()->getEntityManager();
     $guest  = $result->getRepository('AppBundle:usuario')->find($id);

     if ($guest === null) {
       return $this->view("Não há usuarios", Response::HTTP_NOT_FOUND);
     }
     $result->remove($guest);
     $result->flush();

     return $this->view("Usuario removido com sucesso", Response::HTTP_OK);
    }
}
