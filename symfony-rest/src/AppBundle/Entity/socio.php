<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * socio
 *
 * @ORM\Table(name="socio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\socioRepository")
 */
class socio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_usuario", type="integer", unique=true)
     */
    private $idUsuario;

    /**
     * @var int
     *
     * @ORM\Column(name="id_titular", type="integer")
     */
    private $idTitular;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUsuario
     *
     * @param integer $idUsuario
     *
     * @return socio
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return int
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set idTitular
     *
     * @param integer $idTitular
     *
     * @return socio
     */
    public function setIdTitular($idTitular)
    {
        $this->idTitular = $idTitular;

        return $this;
    }

    /**
     * Get idTitular
     *
     * @return int
     */
    public function getIdTitular()
    {
        return $this->idTitular;
    }
}
