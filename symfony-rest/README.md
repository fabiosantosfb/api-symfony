Symfony Standard Edition
========================

ROTAS PARA API HTTP
src/AppBundle/Controller/DefaultController

ENTIDADE PARA TABELAS DO DATA BASE
src/AppBundle/Entity

NÃO E NECESSARIO RODAR ESTA COMANDO POIS AS ENTIDADE JA ESTÃO CRIADAS
php bin/console doctrine:generate:entity
------nome da tabela e campos-----------
AppBundle:nomeTabela


OBS:REQUISO ERA POSTGRES, POREM TODA A FUNCIONALIDADE FORAM TESTA NO MYSQL MINHA MAQUINA MEMORIA E PROCESSAMENTO LIMITADO PARA MAIS SISTEMAS ADICIONAIS

EXISTE DUAS TABELA NO BANCO D DADOS POREM SO ESTOU UTILIZANDO UMA, PELO PRAZO SER CURTO E PELA DIFICULDADE NAS PESQUISAS DOS SOBRE SYMFONY 3 COM DOCTRYNE, EXITEM MUITO COTEUDO DEFAZADO CADA UM DIFERENTE DO OUTROS, POREM A GRADE MAIORIA NÃO SERVIU.  

COMANDO BASICOS PARA GERAR O DATA BASE

php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console server:run


BUSCAR USER GET
http://127.0.0.1:8000/api/v1/usuario/search/2


LISTAR TODOS USUARIOS GET
http://127.0.0.1:8000/api/v1/usuario/list/


REGISTRAR USUARIO POST
http://127.0.0.1:8000/api/v1/usuario/add/
nome
tipo
email
senha

DELETAR USUARIO POR ID DELETE
http://127.0.0.1:8000/api/v1/usuario/remover/1

UPDATE USUARIO PUT POT ID
http://127.0.0.1:8000/api/v1/usuario/update/2
campos que podem ser atualizado
nome
tipo
email
senha

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.0/book/installation.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.0/book/doctrine.html
[8]:  https://symfony.com/doc/3.0/book/templating.html
[9]:  https://symfony.com/doc/3.0/book/security.html
[10]: https://symfony.com/doc/3.0/cookbook/email.html
[11]: https://symfony.com/doc/3.0/cookbook/logging/monolog.html
[13]: https://symfony.com/doc/3.0/bundles/SensioGeneratorBundle/index.html
